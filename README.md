### Jesus Roberto Lopez Arada  

---

# React + Electron + node-adodb = 😍

An example of using react and Electron.

## Scripts
```yarn start``` will start the Electron app and the React app at the same time.  
```yarn build``` will build the React app and package it along the Electron app.

## Read more
## Base on 
You can read more about it in [ Medium article](https://medium.com/@kitze/%EF%B8%8F-from-react-to-an-electron-app-ready-for-production-a0468ecb1da3).
