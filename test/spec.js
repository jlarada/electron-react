
const Application = require('spectron').Application
const assert = require('assert')
const electronPath = require('electron')
const path = require('path')
 
describe('Application launch', function () {
  this.timeout(500000)
 
  beforeEach(function () {
    this.app = new Application({
      path: electronPath,
      args: [path.join(__dirname, '../public/electron.js')]
    })
    return this.app.start()
  })
 
  afterEach(function () {
    if (this.app && this.app.isRunning()) {
      return this.app.stop()
    }
  })
 
  it('Check that initial window is displayed', function () {
    return this.app.client.getWindowCount().then(function (count) {
      assert.equal(count, 1)
    })
  })
  
  it("Check that the correct title is displayed", function() {
    const title = this.app.client.getTitle();
    title.then((value) => {
      assert.equal(value, "React App");
    })
  })  

  it("Check that initial window is visible", function() {
    const visible = this.app.browserWindow.isVisible()
    visible.then((value) => {
      assert.equal(value, true);
    })
  }) 

})