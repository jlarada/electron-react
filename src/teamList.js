import React from "react";  
import {
    Redirect,
    
   } from 'react-router-dom';

const electron = window.require('electron');   
const ipcRenderer = electron.ipcRenderer;

class TeamList extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            redirect: false,
            teams: []
        };

        this.edit = this.edit.bind(this);
        this.delete = this.delete.bind(this);
    }

    edit(e){
        console.log('Editar ', e.target.id);
        ipcRenderer.send('edit-team', e.target.id);
        this.setState({ redirect: true});
    }

    delete(e){
        console.log('Delete', e.target.id);
        ipcRenderer.send('delete-team', e.target.id);
    }

    componentDidMount(){
        ipcRenderer.send('get-list-team');
        ipcRenderer.on('set-list-team', (e, data) => {
            this.setState( {teams: data} );
        });
    }

    render(){
        if(this.state.redirect)
        {
            return (
                <Redirect to={"/new/team"}/>
            );
        }
        let rows = this.state.teams.map(d => {
            return (
                <tr>
                    <td>{d.name}</td>
                    <td>{d.color}</td>
                    <td className="p-3">
                        <button className="btn btn-success m-3" onClick={this.edit} id={d.id}>Edit</button>
                        <button className="btn btn-danger" onClick={this.delete} id={d.id}>Delete</button>
                    </td>
                    
                </tr>
            )
        });
        return (
            <div>
                <h3>Team List</h3>

                <table className="table table-striped">
                    <thead>
                        <tr>
                            <td>Team Name</td>
                            <td>Color</td>
                            <td>Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        { rows }
                    </tbody>
                </table>
            </div>
        )
    }

}

export default TeamList;