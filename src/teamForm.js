import React from "react";
import { Form, Button } from "react-bootstrap";
import Navegacion from "./navBar";

import {     
    Redirect
   } from 'react-router-dom';

const electron = window.require('electron');   
const ipcRenderer = electron.ipcRenderer;

class TeamForm extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            redirect: false,
            id: null,
            name: '',
            color: '',
            error: {}
        };

        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
        this.handleValidation = this.handleValidation.bind(this);
    }

    handleValidation(){
        let valid = true;
        let error = {};
        if(this.state.name === '')
        {
            valid = false;
            error['name'] = 'Field ins required';
        }else{
            if(!/^[a-zA-Z ]+$/.test( this.state.name)){
                valid = false;
                error["name"] = "Only letters";
             }   
        }

        if(this.state.color == '')
        {
            valid = false;
            error['color'] = 'Field is required'
        }else{
            if(!/^[a-zA-Z ]+$/.test( this.state.color)){
                valid = false;
                error["color"] = "Only letters";
             }   
        }

        this.setState( { error: error} );
        return valid;
    }

    componentDidMount(){
        ipcRenderer.on('datos-team-edit', (e, data) => {
            this.setState({ id: data.id});
            this.setState({ name: data.name});
            this.setState({ color: data.color});
        })
    }

    onChange(e){
        // console.log('change value', e.target.name, e.target.value)
        switch (e.target.name) {
            case 'name':
                this.setState({ name: e.target.value });
                break;
        
            case 'color':
                this.setState( { color: e.target.value } )
                break;
        }
    }

    onSubmit(e){
        e.preventDefault();
        // console.log('Enviando ', this.state)
        if(this.handleValidation()){
            ipcRenderer.send('save-datos-teams', this.state);
            this.setState( {redirect: true} );
        }
        
    }

    render(){
        if( this.state.redirect ){
            return (
                <Redirect to={"/"}/>
            );       
        }
        return (
            <div className="p-3">
                
                <Form onSubmit={this.onSubmit}>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" placeholder="Team name here" name="name" value={this.state.name} onChange={this.onChange}/>
                        <span className="text-danger">{this.state.error['name']}</span>                        
                    </Form.Group>

                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Color</Form.Label>
                        <Form.Control type="text" placeholder="Team color here" name="color" value={this.state.color} onChange={this.onChange}/>
                        <span className="text-danger">{this.state.error['color']}</span>                        
                    </Form.Group>

                    <Button variant="primary" type="submit">
                        Submit
                    </Button>
                </Form>
            </div>
        )
    }

}

export default TeamForm;