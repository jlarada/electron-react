import React from "react";

import { Form, Button } from "react-bootstrap";
import {
    Redirect,
    
   } from 'react-router-dom';

const electron = window.require('electron');   
const ipcRenderer = electron.ipcRenderer;

class PositionForm extends React.Component{

    constructor(props){
        super(props);

        this.state = {
            redirect: false,
            id: null,
            position_name: '',
            description: '',
            error: {}
        };

        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.handleValidation = this.handleValidation.bind(this);
    }

    handleValidation(){
        let valid = true;
        let error = {};
        if(this.state.position_name === '')
        {
            valid = false;
            error['position_name'] = 'Field ins required';
        }else{
            if(!/^[a-zA-Z ]+$/.test( this.state.position_name)){
                valid = false;
                error["position_name"] = "Only letters";
             }   
        }

        if(this.state.description == '')
        {
            valid = false;
            error['description'] = 'Field is required'
        }else{
            if(!/^[a-zA-Z ]+$/.test( this.state.description)){
                valid = false;
                error["description"] = "Only letters";
             }   
        }

        this.setState( { error: error} );
        return valid;
    }

    onChange(e){
        // console.log('valor cambiado en ', e.target.name, e.target.value);
        switch (e.target.name) {
            case 'position_name':
                this.setState({ position_name: e.target.value})
                break;
        
            case 'description':
                this.setState({ description: e.target.value})
                break;
        }
    }

    onSubmit(e){
        e.preventDefault();
        // console.log('Enviar el formulario', this.state);
        if(this.handleValidation()){
            ipcRenderer.send('save-datos-position', this.state);
            this.setState( {redirect: true} );
        }
    }

    componentDidMount(){
        ipcRenderer.on('datos-position-edit', (e, data) => {
            this.setState({ id: data.id});
            this.setState({ position_name: data.position_name});
            this.setState({ description: data.description});
        })
    }

    render(){
        if( this.state.redirect ){
            return (
                <Redirect to={"/"}/>
            );       
        }
        return (
            <div className="p-3">
                
                <Form onSubmit={this.onSubmit}>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Position Name</Form.Label>
                        <Form.Control type="text" placeholder="Position Name" name="position_name" value={this.state.position_name} onChange={this.onChange}/>      
                        <span className="text-danger">{this.state.error['position_name']}</span>                  
                    </Form.Group>

                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Description</Form.Label>
                        <Form.Control type="text" placeholder="Description" name="description" value={this.state.description} onChange={this.onChange}/>     
                        <span className="text-danger">{this.state.error['description']}</span>                   
                    </Form.Group>
                    <Button variant="primary" type="submit">
                        Submit
                    </Button>

                </Form>
            </div>
        )
    }
}

export default PositionForm;