import React from "react";
import {
    Redirect,
    
   } from 'react-router-dom';

const electron = window.require('electron');   
const ipcRenderer = electron.ipcRenderer;

class PositionList extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            redirect: false,
            positions: []
        };

        this.edit = this.edit.bind(this);
        this.delete = this.delete.bind(this);
    }

    edit(e){
        console.log('edit');
        ipcRenderer.send('edit-position', e.target.id);
        this.setState( { redirect: true} );
    }

    delete(e){
        console.log('delete');
        ipcRenderer.send('delete-position', e.target.id);
    }

    componentDidMount(){
        ipcRenderer.send('get-list-position');

        ipcRenderer.on('set-list-position', (e, data) => {
            this.setState( { positions: data} )
        });
    }

    render(){
        if(this.state.redirect)
        {
            return (
                <Redirect to={"/new/position"}/>
            );   
        }
        let rows = this.state.positions.map(d => 
            {

                return (
                    <tr>
                        <td>{d.position_name}</td>
                        <td>{d.description}</td>
                        <td>
                            <button className="btn btn-success m-3" onClick={this.edit} id={d.id}>Edit</button>
                            <button className="btn btn-danger" onClick={this.delete} id={d.id}>Delete</button>
                        </td>
                    </tr>
                )
            });
        return (
            <div className="p-3">                
                <h3>Position List</h3>
                <table className="table table-striped">
                    <thead>
                        <tr>
                        <td>Position Name</td>
                        <td>Description</td>
                        </tr>
                    </thead>
                    <tbody>
                        {rows}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default PositionList;