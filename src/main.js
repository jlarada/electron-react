import React from 'react';
import { 
    BrowserRouter as Router,
    Route,
    Switch,
    Link,
    Redirect,
    withRouter
   } from 'react-router-dom';
import TeamList from "./teamList";
import Navegacion from "./navBar";
import { Container } from 'react-bootstrap';

const electron = window.require('electron');   
const ipcRenderer = electron.ipcRenderer;

class Main extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            test: 'test1',
            redirect: false
        };
    }

    componentDidMount(){
        ipcRenderer.on('goto', (e) => {
            this.setState({ redirect: true });
            // console.log('MENSAGE RECIVido ', this.state.test);
        });
    }

    openDB(){
        ipcRenderer.send('open-db');
    }

    render(){
        
        if( this.state.redirect ){
            return (
                <Redirect to={"/second"}/>
            );       
        }
        return (
            
                
            <div className="p-3">

                <h1>Example using react electron and node-adodb</h1>
                {/* <button onClick={ this.openDB } className="btn btn-success">Open db</button> */}
                
                <br></br>
                <TeamList />

                {/* <Link to="/second">Players</Link> */}
            </div>
            
            
        );
    }
}

export default Main;