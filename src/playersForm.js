import React from "react";
import { Form, FormControl,  Button } from "react-bootstrap";
import {     
    Redirect
   } from 'react-router-dom';
import Select from 'react-select';

const electron = window.require('electron');   
const ipcRenderer = electron.ipcRenderer;

const options = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
  ];

class PlayersForm extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            redirect: false,
            id: null,
            firstname: '',
            lastname: '',
            alias: '',
            age: '',
            weigh: '',
            carve: '' ,
            error: {},
            selectedTeam: null,
            teamList: []
        }

        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
        this.handleValidation = this.handleValidation.bind(this);
    }

    handleChange = selectedTeam => {
        this.setState({ selectedTeam });
        console.log(`Option selected:`, selectedTeam);
    };

    handleValidation(){
        let valid = true;
        let error = {};
        if(this.state.firstname === '')
        {
            valid = false;
            error['firstname'] = 'Field ins required';
        }else{
            if(!/^[a-zA-Z ]+$/.test( this.state.firstname)){
                valid = false;
                error["firstname"] = "Only letters";
             }   
        }

        if(this.state.lastname == '')
        {
            valid = false;
            error['lastname'] = 'Field is required'
        }else{
            if(!/^[a-zA-Z ]+$/.test( this.state.lastname)){
                valid = false;
                error["lastname"] = "Only letters";
             }   
        }

        if(this.state.alias == '')
        {
            valid = false;
            error['alias'] = 'Field is required'
        }else{
            if(!/^[a-zA-Z ]+$/.test( this.state.alias)){
                valid = false;
                error["alias"] = "Only letters";
             }   
        }

        if(this.state.age == '')
        {
            valid = false;
            error['age'] = 'Field is required'
        }else{
            if(! /^\d+$/.test( this.state.age)){
                valid = false;
                error["age"] = "Only numbers";
             }   
        }

        if(this.state.weigh == '')
        {
            valid = false;
            error['weigh'] = 'Field is required'
        }else{
            if(! /^\d+$/.test( this.state.weigh)){
                valid = false;
                error["weigh"] = "Only numbers";
             }   
        }

        if(this.state.carve == '')
        {
            valid = false;
            error['carve'] = 'Field is required'
        }else{
            if(! /^\d+$/.test( this.state.carve)){
                valid = false;
                error["carve"] = "Only numbers";
             }   
        }

        this.setState( { error: error} );
        return valid;
    }

    componentDidMount(){
        ipcRenderer.on('datos-players-edit', (e, data) => {
            // console.log('reciviendo datos ', data)
            this.setState( { firstname: data.firstname } );
            this.setState( { lastname: data.lastname } );
            this.setState( { alias: data.alias } );
            this.setState( { age: data.age } );
            this.setState( { weigh: data.weigh } );
            this.setState( { carve: data.carve } );
            this.setState( { id: data.id } );
        });

        ipcRenderer.send('get-list-team');
        ipcRenderer.on('set-list-team', (e, data) => {
            let teams = data.map(d => {
                return {
                    value: d.id,
                    label: d.name
                }
            });
            this.setState(  {teamList: teams} );
        });

        ipcRenderer.on('datos-player-team', (e, data) => {
            this.setState({ selectedTeam: data});
        });
    }

    onChange(e){
        switch (e.target.name) {
            case 'firstname':
                this.setState({ firstname: e.target.value})
                break;
            case 'lastname':
                this.setState({ lastname: e.target.value})
                break;
            case 'alias':
                this.setState({ alias: e.target.value})
                break;
            case 'age':
                this.setState({ age: e.target.value})
                break;    
            case 'weigh':
                this.setState({ weigh: e.target.value})
                break;
            case 'carve':
                this.setState({ carve: e.target.value})
                break;
        }
    }

    onSubmit(e){
        e.preventDefault();
        if(this.handleValidation()){
            ipcRenderer.send('save-datos-players', this.state);

            this.setState( { redirect: true} );
        }
        // console.log('Enviando form', this.state)
    }

    render(){
        if( this.state.redirect ){
            return (
                <Redirect to={"/"}/>
            );       
        }
        return (
            <div className="p-3">
                
                <Form onSubmit={this.onSubmit}>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>First Name</Form.Label>
                        <Form.Control type="text" placeholder="First Name" name="firstname" value={this.state.firstname} onChange={this.onChange}/>
                        <span className="text-danger">{this.state.error['firstname']}</span> 
                    </Form.Group>

                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Last Name</Form.Label>
                        <Form.Control type="text" placeholder="Last Name" name="lastname" value={this.state.lastname} onChange={this.onChange}/>  
                        <span className="text-danger">{this.state.error['lastname']}</span>                   
                    </Form.Group>

                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Alias</Form.Label>
                        <Form.Control type="text" placeholder="Last Name" name="alias" value={this.state.alias} onChange={this.onChange}/>    
                        <span className="text-danger">{this.state.error['alias']}</span>                 
                    </Form.Group>

                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Age</Form.Label>
                        <Form.Control type="text" placeholder="Last Name" name="age" value={this.state.age} onChange={this.onChange}/>           
                        <span className="text-danger">{this.state.error['age']}</span>          
                    </Form.Group>

                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Weigh</Form.Label>
                        <Form.Control type="text" placeholder="Last Name" name="weigh" value={this.state.weigh} onChange={this.onChange}/>    
                        <span className="text-danger">{this.state.error['weigh']}</span>                 
                    </Form.Group>

                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Carve</Form.Label>
                        <Form.Control type="text" placeholder="Last Name" name="carve" value={this.state.carve} onChange={this.onChange}/>     
                        <span className="text-danger">{this.state.error['carve']}</span>                
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Team</Form.Label>
                        <Select
                            value={this.state.selectedTeam}
                            onChange={this.handleChange}
                            options={this.state.teamList}
                        />
                    </Form.Group>

                    <Button variant="primary" type="submit">
                        Submit
                    </Button>
                </Form>
            </div>
        )
    }

}

export default PlayersForm;