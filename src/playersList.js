import React from "react";
import { 
    BrowserRouter as Router,
    Route,
    Switch,
    Link,
    Redirect,
    withRouter
   } from 'react-router-dom';
import Navegacion from "./navBar";
const electron = window.require('electron');   
const ipcRenderer = electron.ipcRenderer;   
//let ADO = electron.remote.getGlobal('ADO');

class PlayersList extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            redirect: false,
            test: 'test1',
            datos: []
        };

        this.edit = this.edit.bind(this);
    }

    componentDidMount(){
        ipcRenderer.send('get-list-player');
        ipcRenderer.on('set-list-player', (e, data) => {
            this.setState({ datos: data });
        });
    }

    delete(e){
        // console.log('eliminar ', e.target.id)
        ipcRenderer.send('delete-players', e.target.id);
    }

    edit(e){
        // console.log('editar', e.target.id);
        ipcRenderer.send('edit-players', e.target.id);
        this.setState( { redirect: true } )
    }

    render(){
        if(this.state.redirect)
        {
            return (
                <Redirect to={"/new/players"}/>
            );   
        }
        let row = this.state.datos.map(r => {
            return (
                <tr key={r.id}>
                    <td>{r.firstname}</td>
                    <td>{r.lastname}</td>
                    <td>{r.alias}</td>
                    <td>{r.age}</td>
                    <td>{r.weigh}</td>
                    <td>{r.carve}</td>
                    <td>
                        <button onClick={this.edit} className="btn btn-success m-3" id={r.id}>Edit</button>
                        <button onClick={this.delete} className="btn btn-danger" id={r.id}>Delete</button>
                    </td>
                   
                </tr>
            )
        })
        return (
            <div className="p-3">
                
                <h1>Players</h1>
                
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <td>First Name</td>
                            <td>Last Name</td>
                            <td>Alias</td>
                            <td>Age</td>
                            <td>Weigth</td>
                            <td>Carve</td>
                            <td>Action</td>
                        </tr>
                    </thead>
                    <tbody>
                    {row}              
                    </tbody>
                </table>
                {/* <Link to="/">BACK</Link> */}
            </div>
            
        );
    }
}

export default PlayersList;