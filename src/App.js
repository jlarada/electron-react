import React, { Component } from 'react';
import { 
  BrowserRouter as Router,
  HashRouter,
  Route,
  Switch,
  Link,
  Redirect,
  withRouter
} from 'react-router-dom';
import "bootstrap/dist/css/bootstrap.css";
import { Container } from "react-bootstrap";
import logo from './logo.svg';
import './App.css';
import Main from './main';
import PlayersList from './playersList';
import PlayersForm from "./playersForm";
import TeamForm from "./teamForm";
import PositionList from "./positionList";
import PositionForm from "./positionForm";
import Navegacion from "./navBar";

const {app} = window.require('electron').remote;

class App extends Component {
  render() {
    return (
      <HashRouter>
        <Container className="col-12">
          <Navegacion />
            <Switch>
              <Route path="/" exact component={Main}/>
              <Route path="/second" exact component={PlayersList}/>
              <Route path="/new/players" exact component={PlayersForm}/>
              <Route path="/new/team" exact component={TeamForm}/>
              <Route path="/new/position" exact component={PositionForm}/>
              <Route path="/list/position" exact component={PositionList}/>
            </Switch>
        </Container>
      </HashRouter>
    );
  }
}

export default App;
