import React from "react";
import { Navbar, Nav, NavDropdown, Button, Form, FormControl } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Main from './main';

const electron = window.require('electron');   
const ipcRenderer = electron.ipcRenderer;

class Navegacion extends React.Component{
    openDB(){
        ipcRenderer.send('open-db');
    }

    appExit(){
        ipcRenderer.send('exit-app');
    }

    render(){
        return(
            <Navbar bg="light" expand="lg" sticky="top">
                <Navbar.Brand >Team DB</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">                    

                    <NavDropdown title="General" id="basic-nav-dropdown">
                        <NavDropdown.Item ><Link to="/list/position">Position List</Link></NavDropdown.Item>
                        <NavDropdown.Item ><Link to="/new/position">New Position</Link></NavDropdown.Item>
                        
                        <NavDropdown.Divider />
                        <NavDropdown.Item onClick={this.openDB}>open db</NavDropdown.Item>
                        <NavDropdown.Item onClick={this.appExit}>exit</NavDropdown.Item>
                    </NavDropdown>

                    <NavDropdown title="Team Manager" id="basic-nav-dropdown">
                        <NavDropdown.Item > <Link to="/">List Team</Link></NavDropdown.Item>
                        <NavDropdown.Item > <Link to="/new/team">New Team</Link></NavDropdown.Item>
                    </NavDropdown>

                    <NavDropdown title="Players Manager" id="basic-nav-dropdown">
                        <NavDropdown.Item ><Link to="/second">Players List</Link></NavDropdown.Item>
                        <NavDropdown.Item><Link to="/new/players">New Player</Link></NavDropdown.Item>                        
                    </NavDropdown>

                    </Nav>
                    
                </Navbar.Collapse>
            </Navbar>
        )
    }
}

export default Navegacion;