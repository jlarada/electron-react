var ADODB = require('node-adodb');
if (process.mainModule.filename.indexOf('app.asar') !== -1) {
  // In that case we need to set the correct path to adodb.js
  ADODB.PATH = './resources/adodb.js';
}
ADODB.debug = true;
// para usar en el acceso a datos
global.ADO = ADODB;
// console.log('ADO',global.ADO);

const electron = require("electron");
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const ipcMain = electron.ipcMain;
const path = require("path");
const isDev = require("electron-is-dev");
const dialog = electron.dialog;
const playerDB = require('./db/player/playerDB.js');
const teamDB = require('./db/team/teamDB.js');
const positionDB = require('./db/position/positionDB.js');

let mainWindow;
let dbName;

/* require("update-electron-app")({
  repo: "kitze/react-electron-example",
  updateInterval: "1 hour"
});
 */

function openDB(){
  dialog.showOpenDialog({
    properties: ['openFile'],
    filters: [
        
        { name: 'Custom File Type', extensions: ['mdb'] },
        { name: 'All Files', extensions: ['*'] }
      ]
    }).then( files => {
        // console.log(files.filePaths[0]);
        // Connect to the MS Access DB
        if(files.filePaths[0] !== undefined){
            //openDB(files.filePaths[0]);
            dbName = files.filePaths[0];
            
            teamDB.loadData(ADODB, dbName).then(d => {
              
              mainWindow.webContents.send('set-list-team', d);  
            });
            //win.webContents.send('actualizar', {});
        }
            
    }).catch(err => {
        console.log(err)
      });
}

function createWindow() {
  mainWindow = new BrowserWindow({ width: 900, height: 680, webPreferences: { nodeIntegration: true, webSecurity: false }});
  mainWindow.loadURL(
    isDev
      ? "http://localhost:3000"
      : `file://${path.join(__dirname, "../build/index.html")}`
  );
  mainWindow.on("closed", () => (mainWindow = null));
  //mainWindow.setMenu(null);
}

app.on("ready", createWindow);

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (mainWindow === null) {
    createWindow();
  }
});

function showMsgOpenDB()
{
  const options = {              
    title: 'Info',
    message: 'Please open db first'             
  };
  dialog.showMessageBox(null, options);
}

ipcMain.on('open-db', (e) =>{
  openDB();
});

ipcMain.on('exit-app', (e) => {
  app.quit();
});



ipcMain.on('save-datos-players', (e, data) => {
  console.log('Datos recividos en electron', data);
  if(dbName)
  {
    if(data.id){
      console.log('actualizar');
      playerDB.update(ADODB, dbName, data.id, data.firstname, data.lastname, data.alias, data.age, data.weigh, data.carve).then(d => {
        console.log('Jugador actualizado', d);
        if(data.selectedTeam){
          playerDB.playerHasTeam(ADODB, dbName, data.id).then(r => {
            if(r.length > 0)
            {
              playerDB.updatePlayerTeam(ADODB, dbName, r[0].id, data.selectedTeam.value, data.id).then(r => console.log('Player Team update')).catch(err => console.log(err));
            }else{
              playerDB.insertPlayerTeam(ADODB, dbName, data.id, data.selectedTeam.value).then(d => console.log('Player team set')).catch(err => console.log(err));  
            }
          });
        }
      });;
    }else{
      console.log('crear uno nuevo');
      playerDB.insert(ADODB, dbName, data.firstname, data.lastname, data.alias, data.age, data.weigh, data.carve).then(d => {
        console.log('Jugador insertado', d);
        playerDB.getLastId(ADODB, dbName, data).then(ids => {
          console.log('Ultimo id insertado', ids);
          if(data.selectedTeam){
            
            playerDB.insertPlayerTeam(ADODB, dbName, ids[0].id, data.selectedTeam.value).then(d => console.log('Player team set')).catch(err => console.log(err));
          }
        }).catch(err => console.log(err))
      });
    }
  }else{
    showMsgOpenDB();
  }
});

ipcMain.on('get-list-player', (e) => {
    if(dbName)
    {
      playerDB.loadData(ADODB, dbName).then(d => {
        // console.log('todos los datos de los players ', d);
        mainWindow.webContents.send('set-list-player', d);  
      });
    }else
    {
      showMsgOpenDB();
    }
});

ipcMain.on('edit-players', (e, id) =>{
  console.log('Edit ', id);

  if(dbName){
    playerDB.get(ADODB, dbName,id).then(d => {
      // console.log('datos del sistema ', d);
      mainWindow.webContents.send('datos-players-edit', {
        id: d[0].id,
        firstname: d[0].firstname,
        lastname: d[0].lastname,
        alias: d[0].alias,
        age: d[0].age,
        weigh: d[0].weigh,
        carve: d[0].carve 
      });
    });

    playerDB.playerHasTeam(ADODB, dbName, id).then(t => {
      if(t.length > 0){
        teamDB.get(ADODB, dbName, t[0].id_team).then(r => {
          mainWindow.webContents.send('datos-player-team', {
            value: r[0].id,
            label: r[0].name
          });
        })
      }
    })
  } else{
    showMsgOpenDB();
  } 
});

ipcMain.on('delete-players', (e, id) =>{
  if(dbName){
    playerDB.del(ADODB, dbName, id).then(d => {
      playerDB.loadData(ADODB, dbName).then(d => {
        // console.log('todos los datos de los players ', d);
        mainWindow.webContents.send('set-list-player', d);  
      });
    });
  }else{
    showMsgOpenDB();
  }
});

ipcMain.on('get-list-team', (e) => {
  if(dbName){
    teamDB.loadData(ADODB, dbName).then(d => {
      // console.log('todos los datos de los players ', d);
      //(dbName)
      mainWindow.webContents.send('set-list-team', d);  
    });
  }
  
});

ipcMain.on('save-datos-teams', (e, data) => {
  console.log('Datos recividos en electron', data);
  if(dbName){
    if(data.id){
      console.log('actualizar');
      teamDB.update(ADODB, dbName, data.id, data.name, data.color).then(d => {
        if(dbName){
          teamDB.loadData(ADODB, dbName).then(d => {
            // console.log('todos los datos de los players ', d);
            //(dbName)
            mainWindow.webContents.send('set-list-team', d);  
          });
        }
      });
    }else{
      console.log('crear uno nuevo');
      teamDB.insert(ADODB, dbName, data.name, data.color).then(d => {
        if(dbName){
          teamDB.loadData(ADODB, dbName).then(d => {
            // console.log('todos los datos de los players ', d);
            //(dbName)
            mainWindow.webContents.send('set-list-team', d);  
          });
        }
      });
    }
  }else{
    showMsgOpenDB();
  }
});

ipcMain.on('edit-team', (e, id) =>{
  console.log('Edit ', id);

  if(dbName){
    teamDB.get(ADODB, dbName, id).then(d => {
      // console.log('datos del sistema ', d);
      mainWindow.webContents.send('datos-team-edit', {
        id: d[0].id,
        name: d[0].name,
        color: d[0].color,
       
      });
    });
  }  else{
    showMsgOpenDB();
  }
});

ipcMain.on('delete-team', (e, id) =>{
  teamDB.del(ADODB, dbName, id).then(d => {
    teamDB.loadData(ADODB, dbName).then(d => {
      // console.log('todos los datos de los players ', d);
      mainWindow.webContents.send('set-list-team', d);  
    });
  });
});

// =========================


ipcMain.on('get-list-position', (e) => {
  if(dbName){
    positionDB.loadData(ADODB, dbName).then(d => {
      // console.log('todos los datos de los players ', d);
      //(dbName)
      mainWindow.webContents.send('set-list-position', d);  
    });
  }else{
    showMsgOpenDB();
  }
  
});

ipcMain.on('save-datos-position', (e, data) => {
  console.log('Datos recividos en electron', data);
  if(dbName){
    if(data.id){
      console.log('actualizar');
      positionDB.update(ADODB, dbName, data.id, data.position_name, data.description);
    }else{
      console.log('crear uno nuevo');
      positionDB.insert(ADODB, dbName, data.position_name, data.description);
    }
  }else{
    showMsgOpenDB();
  }
});

ipcMain.on('edit-position', (e, id) =>{
  console.log('Edit ', id);

  positionDB.get(ADODB, dbName, id).then(d => {
    // console.log('datos del sistema ', d);
    mainWindow.webContents.send('datos-position-edit', {
      id: d[0].id,
      position_name: d[0].position_name,
      description: d[0].description,
     
    });
  });  
});

ipcMain.on('delete-position', (e, id) =>{
  positionDB.del(ADODB, dbName, id).then(d => {
    positionDB.loadData(ADODB, dbName).then(d => {
      // console.log('todos los datos de los players ', d);
      mainWindow.webContents.send('set-list-position', d);  
    });
  });
});