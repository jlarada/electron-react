'use strict';
/* if (process.mainModule.filename.indexOf('app.asar') !== -1) {
    // In that case we need to set the correct path to adodb.js
    ADODB.PATH = './resources/adodb.js';
  }
 */  
//var ADODB = require('node-adodb');
//const ADODB = require('electron').remote.getGlobal('ADODB')

//ADODB.debug = true;


let connection;

const openDB = (ADODB, dbName) =>{
    connection = ADODB.open(`Provider=Microsoft.Jet.OLEDB.4.0;Data Source=${dbName};`);    
}

const close = () => {
    connection.close();
}

const loadData = async (ADODB, dbName) =>{
    openDB(ADODB, dbName);
    if(connection)
    {
        let data = await connection
        .query('SELECT * FROM [players];'); 
        return data;
    }
    close();
    return Promise.reject(Error("DB close"));
}

const insert = async (ADODB, dbName,firstname, lastname, alias, age, weigh, carve ) =>{
    openDB(ADODB, dbName);
    if(connection)
    {
        let query = `INSERT INTO [players](firstname, lastname, alias, age, weigh, carve) VALUES ("${firstname}", "${lastname}", "${alias}", ${age}, ${weigh}, ${carve})`;
        
        let result = connection
        .execute(query);    
        return result;
    }
    close();
    return Promise.reject(Error("DB close"));
}

const update = async (ADODB, dbName, id, firstname, lastname, alias, age, weigh, carve) =>{
    openDB(ADODB, dbName);
    if(connection)
    {
        let query = `UPDATE [players] SET firstname = "${firstname}", lastname = "${lastname}", alias="${alias}", age=${age}, weigh=${weigh}, carve=${carve} WHERE id = ${id}`;
        return connection.execute(query);
    }
    close();
    return Promise.reject(Error("DB close"));
}

const del = async (ADODB, dbName, id) => {
    openDB(ADODB, dbName);
    if(connection)
    {
        let query = `DELETE * FROM [players] WHERE id=${id}`;
        let result = connection.execute(query)
        return result;
    }
    close();
    return Promise.reject(Error("DB close"));
}

const get = async (ADODB, dbName,id) =>{
    openDB(ADODB, dbName);
    if(connection)
    {
        let data = await connection
        .query(`SELECT * FROM [players] WHERE id=${id};`); 
        return data;
    }
    close();
    return Promise.reject(Error("DB close"));
    
}

const getLastId = async (ADODB, dbName, player) =>{
    openDB(ADODB, dbName);
    if(connection){
        //console.log('consulta ', `SELECT * FORM [players] WHERE firstname = "${player.firstname}" & lastname = "${player.lastname}" & alias = "${player.alias}" & age = ${player.age} & weigh = ${player.weigh} & carve = ${player.carve}`);

        let data = await connection.query(`SELECT id FROM [players] WHERE (firstname = "${player.firstname}" AND lastname = "${player.lastname}" AND alias = "${player.alias}" AND age = ${player.age} AND weigh = ${player.weigh} AND carve = ${player.carve})`);
        return data;
    }
    close();
    return Promise.reject(Error("DB close"));
}

const insertPlayerTeam = async (ADODB, dbName, id_player, id_team) => {
    openDB(ADODB, dbName);
    if(connection)
    {
        let result = connection.execute(`INSERT INTO [players_team](id_team, id_player) VALUES (${id_team}, ${id_player})`);
        return result;
    }
    close();
    return Promise.reject(Error("DB close"));
}

const playerHasTeam = async (ADODB, dbName, id_player) =>{
    openDB(ADODB, dbName);
    if(connection){
        let result = connection.query(`SELECT * FROM [players_team] WHERE id_player = ${id_player}`);
        return result;
    }
    close();
    return Promise.reject(Error("DB close"));
}

const updatePlayerTeam = async (ADODB, dbName, id, id_team, id_player) =>{
    openDB(ADODB, dbName);  
    if(connection)
    {
        let result = connection.execute(`UPDATE [players_team] SET id_player = ${id_player}, id_team = ${id_team} WHERE id = ${id}`);
        return result;
    }
    close();
    return Promise.reject(Error("DB close"));
}

//openDB('app-prueba.mdb');
module.exports = { loadData, insert, update, del, get, getLastId, insertPlayerTeam, playerHasTeam, updatePlayerTeam };